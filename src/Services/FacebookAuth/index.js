import initFB from './init'
import axios from 'axios'

class FacebookAuthentication {
  constructor () {
    // initFB((fb, status) => {
    //   this.context = fb
    //   this.status = status
    //   console.log(status)
    // })
    this.init = initFB
    this.connection = {}
    // this.getLoginStatus = this.getLoginStatus.bind(this)
  }

  getLoginStatus () {
    this.context.getLoginStatus((response) => {
      console.log('LOGIN STATUS:', response)
      this.statusChangeCallback(response)
      this.connection = response
    })
  }

  login () {
    this.context.login((response) => {
      this.statusChangeCallback(response)
    }, { scope: 'email public_profile' })
  }

  logout () {
    this.context.logout((response) => {
      this.statusChangeCallback(response)
    })
  }

  authenticateWithServer (status) {
    axios.get(`http://localhost:9021/auth/facebook?access_token=${status.authResponse.accessToken}`, {
      withCredentials: true
    })
      .then(res => console.log(res.data))
      .catch(e => console.log(e))
  }

  statusChangeCallback (response) {
    console.log('*STATUS*', response)
    // this.authenticateWithServer(status)
  }
}

export default FacebookAuthentication
