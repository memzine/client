export default function (cb) {
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1995468017382762',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.12'
    });

    FB.AppEvents.logPageView();

    const customFBObject = {
      getLoginStatus: FB.getLoginStatus,
      login: FB.login,
      logout: FB.logout,
      api: FB.api
    }

    let status
    FB.getLoginStatus((res) => {
      status = res
      cb(customFBObject, status)
    })
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
}
