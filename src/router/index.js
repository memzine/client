import Vue from 'vue'
import Router from 'vue-router'

import AppView from '@/components/Views/AppView'
import LogInView from '@/components/Views/Login'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'DashboardView',
      component: AppView
    },
    {
      path: '/login',
      name: 'LoginView',
      component: LogInView
    }
  ]
})
