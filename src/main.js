// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
// import VueAuthenticate from 'vue-authenticate'

import store from './Store/Store'

Vue.use(vuex)
Vue.use(VueAxios, axios)
// Vue.use(VueAuthenticate, {
//   baseUrl: 'http://localhost:9021',
//   tokenName: 'access_token',
//   storageType: 'cookieStorage',
//   providers: {
//     facebook: {
//       clientId: '1995468017382762',
//       redirectUri: 'http://localhost:8080/',
//       tokenName: 'access_token'
//     }
//   }

//   // bindRequestInterceptor: function () {
//   //   this.$http.interceptors.request.use((config) => {
//   //     console.log('FROM REQUEST BIND: ----')
//   //     console.log(this.getToken())
//   //     if (this.isAuthenticated()) {
//   //       config.data = {
//   //         auth_token: this.getToken()
//   //       }
//   //       console.log(config)
//   //     } else {
//   //       console.log('FROM ELSE', config)
//   //       delete config.headers['Authorization']
//   //     }
//   //     return config
//   //   })
//   // },

//   // bindResponseInterceptor: function () {
//   //   this.$http.interceptors.response.use((response) => {
//   //     console.log('FROM RESPONSE BIND: ----')
//   //     this.setToken(response)
//   //     return response
//   //   })
//   // }
// })

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
